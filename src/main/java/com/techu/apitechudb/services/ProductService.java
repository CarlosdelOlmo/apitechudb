package com.techu.apitechudb.services;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    public List<ProductModel> findAll(){

        return this.productRepository.findAll();
    }

    public ProductModel add(ProductModel product){
        System.out.println("add");

        return this.productRepository.save(product);
    }

    public Optional<ProductModel> findById(String id){
        System.out.println("findById");

        return this.productRepository.findById(id);
    }

    public ProductModel update(ProductModel product){
        System.out.println("update");

        return this.productRepository.save(product);
    }

    public boolean delete(String id){
        System.out.println("delete");

        boolean result = false;

        if (this.productRepository.findById(id).isPresent() == true){
            System.out.println("Producto ENCONTRADO, borrando...");
            this.productRepository.deleteById(id);
            result = true;
        }
        else {
            System.out.println("Producto NO encontrado");
        }

        return result;

    }
}
