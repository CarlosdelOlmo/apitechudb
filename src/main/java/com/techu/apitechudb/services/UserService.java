package com.techu.apitechudb.services;

import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public List<UserModel> findAll(){

        return this.userRepository.findAll();
    }

    public List<UserModel> getUsers(String orderBy){
        System.out.println("getUsers_services");
        //return this.userRepository.findAll(Sort.by(Sort.Direction.ASC, orderBy));
        if(orderBy != null) {
            if (orderBy.equals("age")) {
                return this.userRepository.findAll(Sort.by(Sort.Direction.ASC, "age"));
            }
        }

        return this.userRepository.findAll();
     }

    public List<UserModel> findAllOrderByAgeAsc(){

        return this.userRepository.findAll(Sort.by(Sort.Direction.ASC, "age"));
    }

    public List<UserModel> findAllOrderByAgeDes(){

        return this.userRepository.findAll(Sort.by(Sort.Direction.DESC, "age"));
    }

    public Optional<UserModel> findById(String id){

        System.out.println("findById_userservice, el id es: " + id);
        System.out.println("findById_userservice " + this.userRepository);

        return this.userRepository.findById(id);
    }

    public UserModel add(UserModel user){
        System.out.println("add");

        return this.userRepository.save(user);
    }

    public UserModel update(UserModel user){
        System.out.println("update");

        return this.userRepository.save(user);
    }

    public boolean delete(String id){
        System.out.println("delete");

        boolean result = false;

        if (this.userRepository.findById(id).isPresent() == true){
            System.out.println("Usuario ENCONTRADO, borrando...");
            this.userRepository.deleteById(id);
            result = true;
        }
        else {
            System.out.println("Usuario NO encontrado");
        }

        return result;

    }

}
