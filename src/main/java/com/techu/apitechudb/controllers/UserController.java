package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v3")
@CrossOrigin(origins = "*" , methods = {RequestMethod.GET, RequestMethod.DELETE,RequestMethod.PUT,RequestMethod.POST,RequestMethod.PATCH})
public class UserController {

    @Autowired
    UserService userService;

//    @GetMapping("/users")
//    public ResponseEntity<List<UserModel>> getUsers(){
//        System.out.println("getUsers");
//
//        return new ResponseEntity<>(this.userService.findAll(), HttpStatus.OK);
//    }

    @GetMapping("/users")
//    @CrossOrigin(origins = "*" , methods = {RequestMethod.GET})
    public ResponseEntity<List<UserModel>> getUsers(
            @RequestParam (name = "$orderBy", required = false) String orderBy
    ) {
        System.out.println("getUsers");
        System.out.println("El valor de $orderBy es " + orderBy);
        return new ResponseEntity<>(this.userService.getUsers(orderBy), HttpStatus.OK);
    }

    @GetMapping("/usersOrdByAgeAsc")
    public ResponseEntity<List<UserModel>> getUsersOrdByAgeAsc(){
        System.out.println("getUsersOrdByAgeAsc");

        return new ResponseEntity<>(this.userService.findAllOrderByAgeAsc(), HttpStatus.OK);
    }

    @GetMapping("/usersOrdByAgeDes")
    public ResponseEntity<List<UserModel>> getUsersOrdByAgeDesc(){
        System.out.println("getUsersOrdByAgeDes");

        return new ResponseEntity<>(this.userService.findAllOrderByAgeDes(), HttpStatus.OK);
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable String id){
        System.out.println("getUserById");
        System.out.println("La id del usuario a buscar es: " + id);

        Optional<UserModel> result = this.userService.findById(id);

//        return new ResponseEntity<>(
//                result.isPresent() ? result.get() : "Producto NO encontrado",
//                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
//      );

        if (result.isPresent() == true){
            return new ResponseEntity<>(result.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Usuario NO encontrado",HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel user){
        System.out.println("addUsers");
        System.out.println("La id del usuario que se va a crear es: " + user.getId());
        System.out.println("El nombre del usuario que se va a crear es: " + user.getName());
        System.out.println("La edad del usuario que se va a crear es: " + user.getAge());

        return new ResponseEntity<>(this.userService.add(user),HttpStatus.CREATED);

    }

    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> updateUser(@RequestBody UserModel userModel,@PathVariable String id){
        System.out.println("updateUsers");
        System.out.println("El id del usuario que se va a actualizar en parámetro es: " + id);
        System.out.println("El id del usuario que se va a actualizar: " + userModel.getId());
        System.out.println("El nombre del usuario que se va a actualizar es: " + userModel.getName());
        System.out.println("La edad del usuario que se va a actualizar es: " + userModel.getAge());

        Optional<UserModel> userToUpdate = this.userService.findById(id);

        if (userToUpdate.isPresent()){
            System.out.println("Usuario para actualizar ENCONTRADO!!, actualizando...");
            this.userService.update(userModel);
        } else {
            System.out.println("Usuario para actualizar NO ENCONTRADO!!");
        }

        return new ResponseEntity<>(userModel,
                userToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);

    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable String id){
        System.out.println("deleteUser");
        System.out.println("El id del usuario a borrar es : " + id);

        boolean deleteUser = this.userService.delete(id);

        return new ResponseEntity<>(
                deleteUser ? "Usuario Borrado" : "Usuario NO borrado",
                deleteUser ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

}
